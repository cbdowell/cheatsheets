LSRC(1)                                     BSD General Commands Manual                                     LSRC(1)

NAME
     lsrc — show dotfiles files managed by rcm

SYNOPSIS
     lsrc [-FhqVv] [-B hostname] [-d dir] [-I excl_pat] [-S excl_pat] [-s excl_pat] [-t tag] [-U excl_pat]
          [-u excl_pat] [-x excl_pat] [files ...]

DESCRIPTION
     This program lists all configuration files, both the sources in the dotfiles directories and the destinations
     in your home directory.  See rcup(1), the DIRECTORY LAYOUT section, for details on the directory layout.  It
     supports these options:

     -B HOSTNAME  treat host-HOSTNAME as the host-specific directory instead of computing it based on the com‐
                  puter's hostname

     -d DIR       list dotfiles from the DIR. This can be specified multiple times.

     -F           show symbols next to each file indicating status information. Supported symbols are @ which indi‐
                  cates that the file is a symlink, $ which indicates it's a symlinked directory, and X to indicate
                  that the file is a copy. More details on copied files and symlinked directories can be found in
                  rcrc(5) under the documentation on COPY_ALWAYS and SYMLINK_DIRS, respectively.

     -h           show usage instructions.

     -I excl_pat  include the files that match the given pattern. This is applied after any -x options. It uses the
                  same pattern language as -x; more details are in the EXCLUDE PATTERN section. Note that you may
                  have to quote the exclude pattern so the shell does not evaluate the glob.

     -S excl_pat  symlink the directories that match the given pattern. See EXCLUDE PATTERN for more details. This
                  option can be repeated. You may need to quote the pattern to prevent the shell from swallowing
                  the glob.

     -s excl_pat  if a directory matches the given pattern, recur inside of it instead of symlinking. See EXCLUDE
                  PATTERN for more details. This is the opposite of the -S option, and can be used to undo it or
                  the SYMLINK_DIRS setting in your rcrc(5) configuration. It can be repeated, and the pattern may
                  need to be quoted to protect it from your shell.

     -t TAG       list dotfiles according to TAG

     -U excl_pat  the rc files or directories matching this pattern will not be symlinked or created with a leading
                  dot. See EXCLUDE PATTERN for more details. This option can be repeated. You may need to quote the
                  pattern to prevent the shell from swallowing the glob.

     -u excl_pat  if an rc file or directory matches the given pattern, it must be dotted. See EXCLUDE PATTERN for
                  more details. This is the opposite of the -U option, and can be used to undo it or the UNDOTTED
                  setting in your rcrc(5) configuration. This option can be repeated. You may need to quote the
                  pattern to prevent the shell from swallowing the glob.

     -V           show the version number.

     -v           increase verbosity. This can be repeated for extra verbosity.

     -q           decrease verbosity

     -x excl_pat  exclude the files that match the given pattern. See EXCLUDE PATTERN for more details. This option
                  can be repeated. Quote the pattern if it contains a valid shell glob.

     files ...    only list the specified file(s)

EXCLUDE PATTERN
     The exclude pattern specifies a colon-separated pair of dotfiles directory and file glob. The dotfiles direc‐
     tory is optional and, if omitted, defaults to *, which is a special token that matches any dotfiles directory.
     The file glob is relative to the dotfiles directory, ignoring meta directories. A colon combines them.

     For example, to ignore all emacs-related items from the thoughtbot-dotfiles directory, use the exclude pat‐
     tern:

           thoughtbot-dotfiles:*emacs*

     To ignore any bash_profile file, use the pattern:

           *:bash_profile

     Or more simply:

           bash_profile

     Since exclude patterns are often valid shell globs, be sure to quote them. See the caveats noted in BUGS when
     using an exclude pattern.

ENVIRONMENT
     RCRC  User configuration file. Defaults to ~/.rcrc.

FILES
     ~/.dotfiles ~/.rcrc

SEE ALSO
     mkrc(1), rcdn(1), rcup(1), rcrc(5), rcm(7)

AUTHORS
     lsrc is maintained by Mike Burns <mburns@thoughtbot.com> and thoughtbot: http://thoughtbot.se

BUGS
     For macOS systems, we strongly encourage the use of the HOSTNAME variable in your rcrc(5).  We use the
     hostname(1) program to determine the unique identifier for the host. This program is not specified by POSIX
     and can vary by system. On macOS the hostname is unpredictable, and can even change as part of the DHCP hand‐
     shake.

     There are a few bugs around shell globs. Anything involving an exclude pattern is unpredictable, so use -v
     when dealing with patterns. Specifically, globs may expand at any time and remain expanded for the duration of
     the run, which means they cannot be applied more than once.

BSD                                              December 23, 2016                                              BSD
